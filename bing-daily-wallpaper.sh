#!/bin/sh

set -eu

# https://github.com/TadoTheMiner/bing-wallpaper-server/
# rewrite without http server with only coreutils and wget

filename="bing-daily-wallpaper.jpeg"
filepath="$(realpath "$(dirname "$0")")/Wallpapers"
fullpath="$filepath/$filename"

baseurl="https://bing.com"
jsonurl="/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=en-US"

if ! [ -s "$fullpath" ] \
|| [ "$(stat -c '%w' "$fullpath" | cut -d ' ' -f 1)" != "$(date +"%F")" ]
then
	imgurl="$(wget "$baseurl/$jsonurl" -O- \
		| grep -Po '"url":".*?"' \
		| sed -r 's#.*"(.*)"#\1#')"

	wget "$baseurl$imgurl" -O "$fullpath"
fi
