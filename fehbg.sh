#!/bin/bash

set -eu

case "$(hostname)" in
	paskakasa|paskakaari)
		feh --no-fehbg --bg-scale "$HOME"/.config/feh/Wallpapers/black_background.png
		exit 0
		;;
	*)
		;;
esac

# In POSIX sh, RANDOM is undefined. Use bash.
case $((1 + RANDOM % 7)) in
	1)
		feh --no-fehbg --bg-scale "$HOME"/.config/feh/Wallpapers/Moti1.jpg
		;;
	2)
		feh --no-fehbg --bg-scale "$HOME"/.config/feh/Wallpapers/Moti2.jpg
		;;
	3)
		feh --no-fehbg --bg-scale "$HOME"/.config/feh/Wallpapers/saimaa.jpg
		;;
	4)
		feh --no-fehbg --bg-scale "$HOME"/.config/feh/Wallpapers/smiley.jpg
		;;
	5)
		feh --no-fehbg --bg-scale "$HOME"/.config/feh/Wallpapers/copium.png
                ;;
	6)
		feh --no-fehbg --bg-scale "$HOME"/.config/feh/Wallpapers/Windows.png
                ;;
	*)
		feh --no-fehbg --bg-scale "$HOME"/.config/feh/Wallpapers/copium.png
		;;
esac
